from fastapi import Depends, FastAPI
from typing import List
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
import uvicorn
from schemas import User, UserBase, UserCreate

Base = declarative_base()
app = FastAPI()

SQLALCHEMY_DATABASE_URL = "postgresql://postgres:ds76TRmn@localhost:5432/fastapitask"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/users/", response_model=List[User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = db.query(User).offset(skip).limit(limit).all()
    return users


@app.get("/users/filter/", response_model=List[User])
def read_filtered_users(gender: str, limit: int = 100, db: Session = Depends(get_db)):
    users = db.query(User).filter(User.gender == gender).limit(limit).all()
    return users

    
@app.post("/users/", response_model=User)
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    db_user = User(name=user.name, gender=user.gender)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


if __name__ == "__main__":
    Base.metadata.create_all(bind=engine)
    uvicorn.run(app, host="0.0.0.0", port=8000)
